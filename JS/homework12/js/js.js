let images = document.querySelector('.images-wrapper');

for (let i = 0; i <images.children.length; i++){
    images.children[i].dataset.index = i;
    if(i !== 0)
        images.children[i].hidden = true;
}

let timerInterval;
let interval;


function startInterval(){
    timer.innerText = "10.00";
    timerInterval = setInterval(() => {
        timer.innerText = (timer.innerText - 0.01).toFixed(2);
    }, 10)


    interval = setInterval(() => {
        timer.innerText = "10.00";
        let curr = images.querySelector('img:not([hidden])');
        curr.hidden = true;
        if (curr === images.lastElementChild){
            images.firstElementChild.hidden = false;
        }else{
            images.children[+curr.dataset.index + 1].hidden = false;
        }
    }, 10000)


};



let stopBtn = document.createElement('button');
stopBtn.classList.add("stop-btn");
let resumeBtn = document.createElement('button');
resumeBtn.classList.add("resume-btn");
let timer = document.createElement('p');

stopBtn.innerText = "Stop";
resumeBtn.innerText = "Resume";
resumeBtn.disabled = true;

stopBtn.style.backgroundColor = '#F08770';
stopBtn.style.borderRadius = '5px';
stopBtn.style.padding = '10px';
stopBtn.style.fontSize = '14px';
stopBtn.style.fontWeight = 'bold';

resumeBtn.style.backgroundColor = '#A0F070';
resumeBtn.style.borderRadius = '5px';
resumeBtn.style.padding = '10px';
resumeBtn.style.fontSize = '14px';
resumeBtn.style.fontWeight = 'bold';




stopBtn.addEventListener('click', stop);

function stop() {
     clearInterval(timerInterval);
    clearInterval(interval);
    resumeBtn.disabled = false;
    stopBtn.disabled = true;
}

resumeBtn.addEventListener('click', resume);

function resume() {
    startInterval();
    resumeBtn.disabled = true;
    stopBtn.disabled = false;
}

document.body.appendChild(timer);
document.body.appendChild(stopBtn);
document.body.appendChild(resumeBtn);

startInterval();
