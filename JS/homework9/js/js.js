window.onload = function () {

    document.querySelector('.tabs').addEventListener('click', showTabs);
    function showTabs(event) {
        if (event.target.className == 'tabs-title'){
            let dataTab = event.target.getAttribute('data-tab');
            // debugger
            let tabTitle = document.getElementsByClassName('tabs-title');
            for (let i =0; i<tabTitle.length; i++ ) {
                tabTitle[i].classList.remove('active');
            }
            event.target.classList.add('active');
            let tabsItems = document.getElementsByClassName('tabs-content--item');
            for (let i = 0; i<tabsItems.length; i++ ){
                // debugger
                if (dataTab==i ){
                    tabsItems[i].style.display = 'block';
                }
                else {
                    tabsItems[i].style.display = 'none';
                }
            }
        }

    }
};