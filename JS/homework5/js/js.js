function createNewUser() {
    let newUser = {
        firstName: prompt("enter your name"),
        lastName: prompt("enter your surname"),
        birthDate: new Date(prompt("enter your date of birth (mm.dd.yyyy)")),

        getAge: function () {
            let today = new Date();
            let age = today.getFullYear() - newUser.birthDate.getFullYear();
            let m = today.getMonth() - newUser.birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < newUser.birthDate.getDate())) {
                age = age - 1;
            }
            return age;
        },

        getPassword: function () {
            return newUser.firstName.charAt(0).toLocaleUpperCase() + newUser.lastName.toLocaleLowerCase() + newUser.birthDate.getFullYear()
        }


    };

    return newUser;
}



let obj = createNewUser();

console.log(obj);

console.log(obj.birthDate);
console.log(obj.getAge());

console.log(obj.getPassword());


