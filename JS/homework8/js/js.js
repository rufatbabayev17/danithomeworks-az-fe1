let input = document.querySelector('#input-price');


input.addEventListener("focus", onFocus);


function onFocus() {
    this.style.border = '2px solid #23eb52';
    input.style.outline = 'none';
}


input.addEventListener("focusout", focusOut);

function focusOut() {
    if (+input.value < 0) {
        input.style.border = '2px solid red';
        input.style.outline = 'none';
    }

    if (input.value == ''){
        error.remove();
        button.parentElement.remove();
    }
}



let div = document.createElement('div');
let span = document.createElement('span');
let button = document.createElement('button');


let error = document.createElement('p');
error.innerText = "Please enter correct price";


input.addEventListener('change', changeValue);

function changeValue() {
    if(+input.value >= 0){
        span.innerText = `Current Price : ${input.value + "$"}`;
        div.appendChild(span);
        button.innerText = 'X';

        button.addEventListener("focus", onFocus);

        function onFocus() {
            button.parentElement.remove();
            error.remove();
        }

        div.appendChild(button);
        document.body.insertBefore(div, document.body.children[2]);
        input.style.color = 'green';
        if (error) {
            error.remove();
            input.style.border = ""
        }

    }else{
        document.body.appendChild(error);
        input.style.border = '2px solid red';
        input.style.outline = 'none';
        if(button.parentElement)
            button.parentElement.remove();
    }

}