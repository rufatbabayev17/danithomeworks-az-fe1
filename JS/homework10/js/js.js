let icons = document.getElementsByClassName("icon-password");


for (let icon of icons) {
    icon.addEventListener("click", onClick);

    function onClick() {
        if (icon.classList.contains("fa-eye")) {
            icon.previousElementSibling.type = "text";
            icon.classList.remove("fa-eye");
            icon.classList.add("fa-eye-slash");
        } else {
            icon.previousElementSibling.type = "password";
            icon.classList.remove("fa-eye-slash");
            icon.classList.add("fa-eye");
        }
    }
}


let inputCompare = document.getElementById('input-compare');
let inputs = document.getElementsByTagName('input');

let error = document.createElement('span');
error.innerHTML = "Нужно ввести одинаковые значения";
error.style.color = "red";




inputCompare.addEventListener('click', inpComp);
function inpComp(e){
    e.preventDefault();
    if(inputs[0].value === inputs[1].value){
        error.remove();
        alert("You are welcome");
    }else{
        inputs[1].parentNode.parentNode.appendChild(error);
    }
}