const tabs = $('.tabs');
const tabsItemsContainer = $('.tabs-content');
const tabsItems = $('.tabs-content-item');


tabs.on('click', '.tabs-title', function() {

    tabsItemsContainer.css({
        height: tabsItemsContainer.height()
    });

    $('.active').removeClass('active');
    $(this).addClass('active');
    $(tabsItems[$(this).index()]).addClass('active');
})